# The Classerator #

You need Groovy installed to run this. A good way to do that is to install [SDKMan](https://sdkman.io),
then install Groovy from there:

```
$ sdk install groovy
$ groovy -version
Groovy Version: 3.0.8 JVM: 1.8.0_302 Vendor: Azul Systems, Inc. OS: Mac OS X
```

Now you can run the classerator:

```
$ groovy Classerator.groovy --help
```

The first time you run this it will take a little bit because it has download some jar files. Subsequent runs
should be fairly quick.

You can do some useful things like generate serializer and deserializer classes:

```
$ groovy Classerator.groovy --generate-all --destination=code
```

That will write all of the classes to the folders **code/serializers** and **code/deserializers**.

Get all the properties for every class:

```
$ groovy Classerator.groovy --print-methods
```

Get the class hierarchy for the classes:

```
$ groovy Classerator.groovy --print-hierarchy
```

