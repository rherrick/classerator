//file:noinspection DuplicatedCode

import groovy.text.GStringTemplateEngine
import groovy.transform.Field
import org.apache.commons.lang3.StringUtils
import org.nrg.xdat.base.BaseElement
import org.reflections.Reflections
import org.reflections.scanners.SubTypesScanner
import picocli.CommandLine.Command
import picocli.CommandLine.Option
import picocli.CommandLine.Parameters
import picocli.groovy.PicocliScript2

import java.lang.reflect.Modifier
import java.util.function.Supplier
import java.util.regex.Pattern
import java.util.stream.Collectors

@GrabResolver(name = "Maven Central", root = "https://repo.maven.apache.org/maven2")
@GrabResolver(name = "NRG Release Repo", root = "https://nrgxnat.jfrog.io/nrgxnat/libs-release")
@GrabResolver(name = "NRG Snapshot Repo", root = "https://nrgxnat.jfrog.io/nrgxnat/libs-snapshot")
@Grapes([
        @Grab("com.google.guava:guava:20.0"),
        @Grab("info.picocli:picocli-groovy:4.6.1"),
        @Grab("org.apache.commons:commons-lang3:3.11"),
        @Grab("org.nrg.xnat:xnat-data-models:1.8.3-SNAPSHOT"),
        @Grab("org.reflections:reflections:0.9.12")
])
@GrabConfig(systemClassLoader = true)

@Command(name = "Classerator",
        mixinStandardHelpOptions = true,
        version = "1.0",
        description = "@|bold Classerator|@ script")
@PicocliScript2

@Option(names = ["-f", "--file"], description = "Indicates a file containing the list of classes to be processed")
@Field File file

@Option(names = ["-l", "--limit"], description = "Limits the output to display only those classes that are not also subclassed by other classes", defaultValue = "false")
@Field boolean limited

@Option(names = ["-gs", "--generate-serializers"], description = "Should generate serializer classes for each data type", defaultValue = "false")
@Field boolean generateSerializers

@Option(names = ["-gd", "--generate-deserializers"], description = "Should generate deserializer classes for each data type", defaultValue = "false")
@Field boolean generateDeserializers

@Option(names = ["-ga", "--generate-all"], description = "Should generate serializer and deserializer classes for each data type (equivalent to --generate-serializers --generate-deserializers)", defaultValue = "false")
@Field boolean generateAll

@Option(names = ["-d", "--destination"], description = "If generating serializer and/or deserializer classes for each data type, this specifies the destination folder for the generated classes. Note that this should be the @|italic parent|@ folder of the @|bold serializers|@ and @|bold deserializers|@ packages, as files will be written into those subfolders.")
@Field File destination

@Option(names = ["-ph", "--print-hierarchy"], description = "Print out list of classes with subclasses", defaultValue = "false")
@Field boolean printHierarchy

@Option(names = ["-pm", "--print-methods"], description = "Print out list of classes with subclasses", defaultValue = "false")
@Field boolean printMethods

@Option(names = ["-p", "--print-all"], description = "Print out list of classes with subclasses", defaultValue = "false")
@Field boolean printAll

@Option(names = ["-v", "--verbose"], description = "Generate extra output and processing info", defaultValue = "false")
@Field boolean verbose

@Parameters(index = "0", description = "The class to describe.", defaultValue = "")
@Field String describeClass

static Set<Class<?>> getSupers(Class<?> subclass) {
    Pattern isAutoOrBase = ~/^(Auto|Base).*$/
    Set<Class<?>> classes = []
    Class<?> parent = subclass.superclass
    do {
        if (!isAutoOrBase.matcher(parent.simpleName).matches()) {
            classes << parent
        }
        parent = parent.superclass
    } while (parent != null && parent != BaseElement)
    classes
}

static Map<String, String> getMethods(Class<?> clazz, Set<Class<?>> superclasses) {
    final Pattern getter = ~/^get[A-Z][A-z0-9_]+(?<!FK)$/
    final List<String> excludedMethods = ["getSchemaElementName", "getAddParametersByName", "getDefaultIdentifier", "getScannerDelayComparator"]

    clazz.superclass.superclass.declaredMethods.findAll { method ->
        method.parameterCount == 0 && Modifier.isPublic(method.modifiers) && !Modifier.isStatic(method.modifiers) && getter.matcher(method.name).matches() && !excludedMethods.contains(method.name) && !superclasses.contains(method.returnType)
    }.collectEntries { method ->
        [(method.name - "get").uncapitalize(), method.returnType.name - "java.lang."]
    }.sort()
}

void info(Supplier<String> supplier) {
    info(supplier.get())
}

void info(String output) {
    if (verbose) {
        println output
    }
}

//noinspection GroovyVariableNotAssigned
def hasClassname = StringUtils.isNotBlank(describeClass)
def hasFile = file != null

if (hasFile && !file.exists()) {
    println "Can't find the file ${file.path}"
    System.exit 2
}

if (destination != null && !destination.exists()) {
    println "The specified destination folder ${destination.path} does not exist"
    System.exit 2
}

if (generateAll) {
    generateSerializers = generateDeserializers = true
    info "Generating serializers and deserializers"
} else if (generateSerializers) {
    info "Generating serializers only"
} else if (generateDeserializers) {
    info "Generating deserializers only"
}

if (printAll) {
    printHierarchy = printMethods = true
}

Set<String> classnames = []
Pattern validClassname = ~/^(org\.nrg\.xdat\.om\.)?[A-Z][^.]+/

if (!hasClassname && !hasFile) {
    // If no class name or file specified, then just get everything from org.nrg.xdat.om that extends BaseElement
    Set<Class<? extends BaseElement>> subtypes = new Reflections("org.nrg.xdat.om", new SubTypesScanner(true)).getSubTypesOf BaseElement.class
    classnames.addAll subtypes.findAll { validClassname.matcher(it.name).matches() }.collect { it.name }
} else if (hasClassname) {
    classnames << describeClass
} else if (hasFile) {
    classnames.addAll file.readLines()
}

println "Found ${classnames.size()} classes"
info () -> classnames.collect { classname -> " * ${classname}" }.stream().collect(Collectors.joining("\n"))

Set<String> invalids = classnames.findAll { !validClassname.matcher(it).matches() }.collect()
if (!invalids.isEmpty()) {
    println "You must specify a plain class name (e.g. XnatMrsessiondata) or a fully qualified class name in the \"org.nrg.xdat.om\" package (e.g. org.nrg.xdat.om.XnatMrsessiondata)."
    println "The following classes didn't match:\n"
    invalids.forEach { println " * ${it}" }
    System.exit 3
}

final Map<Class<?>, Set<Class<?>>> hierarchies = classnames.sort().collect {
    Class.forName(StringUtils.prependIfMissing(it, "org.nrg.xdat.om"))
}.collectEntries {
    [it, getSupers(it)]
}

//noinspection GroovyVariableNotAssigned
final Set<Class<?>> subclassed = hierarchies.values().stream().flatMap(Collection::stream).collect(Collectors.toSet())

if (printHierarchy || printMethods) {
    hierarchies.keySet().findAll { key ->
        !limited || !subclassed.contains(key)
    }.each { key ->
        println "${key.name}:"
        if (printHierarchy) {
            hierarchies[key].each { println " -> ${it.name}" }
        }
        if (printMethods) {
            getMethods(key, hierarchies[key]).each { println " * ${it.key} returns ${it.value}" }
        }
    }
}

if (generateSerializers || generateDeserializers) {
    def baseSerializer = new File('BaseSerializer.template')
    def primarySerializer = new File('PrimarySerializer.template')
    def baseDeserializer = new File('BaseDeserializer.template')
    def primaryDeserializer = new File('PrimaryDeserializer.template')
    def engine = new GStringTemplateEngine()

    File serializerRoot
    File deserializerRoot
    if (destination != null) {
        serializerRoot = destination.toPath().resolve("serializers").toFile()
        deserializerRoot = destination.toPath().resolve("deserializers").toFile()
    } else {
        serializerRoot = new File("serializers")
        deserializerRoot = new File("deserializers")
    }
    [serializerRoot, deserializerRoot].findAll() { !it.exists() }.each { it.mkdir() }

    hierarchies.entrySet().each { entry ->
        final Class<?> key = entry.key
        final Set<Class<?>> superclasses = entry.value
        Map<String, String> methods = getMethods(key, superclasses)

        def binding = [classname : key.simpleName,
                       superclass: key.superclass.superclass.superclass.simpleName]

        if (generateSerializers) {
            def name = "${key.simpleName}Serializer.java"
            println "Generating serializer class ${name} for ${key.simpleName}"
            binding.comment = methods.isEmpty() ? "// No class-specific properties to serialize" : "// TODO: Implement datatype-specific serialization"
            binding.methods = methods.entrySet().collect { method ->
                switch (method.value) {
                    case "String":
                        return "        writeNonBlankField(generator, \"${method.key}\", instance.get${method.key.capitalize()}());\n"
                    case "Boolean":
                        return "        writeNonNullBoolean(generator, \"${method.key}\", instance.get${method.key.capitalize()}());\n"
                    case "Integer":
                    case "Double":
                        return "        writeNonNullNumber(generator, \"${method.key}\", instance.get${method.key.capitalize()}());\n"
                    default:
                        return "        // TODO: Write out the \"${method.key}\" property here: ${method.value}\n"
                }
            }
            def template = engine.createTemplate(superclasses.isEmpty() ? baseSerializer : primarySerializer).make(binding)
            def target = new File(serializerRoot, name)
            if (!target.exists()) {
                target.withPrintWriter { writer ->
                    writer.println(template.toString())
                }
            } else {
                println("The target file ${name} already exists at the destination ${serializerRoot}, skipping")
            }
        }
        if (generateDeserializers) {
            def name = "${key.simpleName}Deserializer.java"
            println "Generating deserializer class ${name} for ${key.simpleName}"
            binding.comment = methods.isEmpty() ? "// No class-specific properties to deserialize" : "// TODO: Implement datatype-specific deserialization"
            binding.methods = methods.entrySet().collect { method ->
                switch (method.value) {
                    case "String":
                        return "            case \"${method.key}\":\n                instance.set${method.key.capitalize()}(parser.getText());\n                break;\n"
                    case "Boolean":
                        return "            case \"${method.key}\":\n                instance.set${method.key.capitalize()}(parser.getBooleanValue());\n                break;\n"
                    case "Integer":
                        return "            case \"${method.key}\":\n                instance.set${method.key.capitalize()}(parser.getIntValue());\n                break;\n"
                    case "Double":
                        return "            case \"${method.key}\":\n                instance.set${method.key.capitalize()}(parser.getDoubleValue());\n                break;\n"
                    default:
                        return "            case \"${method.key}\":\n                // TODO: Handle the \"${method.key}\" property here: ${method.value}\n                break;\n"
                }
            }
            def template = engine.createTemplate(superclasses.isEmpty() ? baseDeserializer : primaryDeserializer).make(binding)
            def target = new File(deserializerRoot, name)
            if (!target.exists()) {
                target.withPrintWriter { writer ->
                    writer.println(template.toString())
                }
            } else {
                println("The target file ${name} already exists at the destination ${deserializerRoot}, skipping")
            }
        }
    }
}